#!vim
# Destribution under gnu public licence gplv2.0 
# Attension : please be awhere that by using the docker version of the setup
# script you agree to all google
# sdk licences as if not we can not install the sdk without user interaction
# 
# This is a project to make android developement on the commandline more
# efficient, easier and enjoiable.
# You can use it to install your android sdk environement
# for instance by executing ./setupAndroid-sdk.sh
# A new folder .android-sdk will be created in your $HOME directory in which the
# android sdk tools will be stored.
# Please be sure to have up to 35 GB of free diskspace available because many
# old android api levels will be downloaded before the newer ones
# YOu can delete them later if you do not need them any more
# --------------------------------------------------------------------------
# To create new android projects
# -----------------------------------
# The android project creater acts like the creation part of modern IDEs and
# asks you for the required information to create a new project
# You do not have to remember all parameters and their order.
# But it is good to know what the single parameters mean to project creation to
# make good decissions of application design
# -----------------------------------------
# installation
# just execute the androidProjectCreater.sh file
# If the android sdk tools are not allready found in your $PATH it will ask you
# for the absolute path where the android command is stored
# Your actualized $PATH will be stored in a new directory .androidProjectCreater
# in your $HOME directory
# If you havn't installed the android sdk tools yet please execute
# ./setupAndroid-sdk.sh to install it
# ------------------
# For compilation of your projects you will need the apache-ant package. It can
# be found in the most distributions repositories or compiled from source with
# small effort
# 
# ##########################################################################
#
#  Update !
# In order to use the android sdk cli with an actual distribution you have to
# use the java openjdk-8-jdk instead of the version 11.0
# TO make this possible with a small effort, I created a Dockerfile you can
# build and tag to debian-blind-android-project-creater 
# After doing that, with the other stepps allready completed you can just run
# the ./run-android-project-creater.sh to start the docker image you created and
# taggt
# THe stepps for that are :
# Go to the docker folder 
# > cd ./docker-install/ 
# Build and tag the Docker image from the Dockerfile 
# YOu may have to execute this as root
# > docker build -t debian-blind-android-project-creater 
# run the shell script, you may have to execute this as root
# >  ./run-android-project-creater.sh 
# Hint : executable lines are marked by ">" sign. 
