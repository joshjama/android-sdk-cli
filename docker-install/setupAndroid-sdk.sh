#!/bin/bash
#preset:
# openjdk-8 installed and java home path working
# 
#How to setup the android sdk and build-tools
# use the old, as deprecated marked android sdk with the android command which
#is  capable of project-creation for the moment it can e downloaded from
# http://dl.google.com/android/android-sdk_r20-linux.tgz
# just execute this short script to install it automaticaly
# YOu will have to interrupt it if it echos that there is no more to install
# because the android update sdk command has no valid return values
mkdir -p $HOME/.android-sdk
cd $HOME/.android-sdk
wget http://dl.google.com/android/android-sdk_r20-linux.tgz
tar -xf android-sdk_r20-linux.tgz
#After downloading the zip file and extracting it
# Download all android sdk api levels needed for android developement and
# install them
while true
do
echo "yes" | $HOME/.android-sdk/android-sdk-linux/tools/android update sdk --all --no-ui  | grep "There is nothing to install or update"
if [[ $? -eq 0 ]]
then
  break
fi
done
#If repos are not found, try to execute it with UI as following:
#/PATH/TO/EXTRACTED/tools/android update sdk --all
#It opens on the gtk-UI it's accessible by orca
# try to tick the accept box and install all needed dependencies (build-tools,
#platformtools )
#If not working, or no GUI available:
# Try to use the actual sdk-tools from google to manage needed buildtools and
#platform tools
# navigate to the developer page where you can download android studio and
#select sdk-tools only
#Alternatively :
#wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
# use the sdkmanager at 
#PATH/TO/EXTRACTEd/sdkmanager 
# to list available packages:
#PATH/TO/EXTRACTEd/sdkmanager  list sdk
# to install specific package packagename in quotes
#PATH/TO/EXTRACTEd/sdkmanager "packagename"
#Take the needed build-tools and platform-tools -number and revision from the
#android-version list from wikipedia
#wget https://en.wikipedia.org/wiki/Android_version_history

# After successfully installing build-tools and platform-tools with the
#sdkmanager, try again to update the old android tools with 
# android update sdk --no-ui
# Be sure to use the deprecated android command instead of the new rapper comand
#from the sdkmanager because it's the only one which can create new projects 
#/PATH/TO/EXTRACTED/tools/android update sdk --all --no-ui
#If it doesn't work, try it again with the gui version, ore use the new one and
#clone an example project to start fore example you can use the following git
#repo to start a new android 6 project
#git clone https://bitbucket.org/joshjama/example_android_6.git
#Install the ant tools to build your app automaticly. It should be provided in
#the most distributions repos.
# sudo apt-get install ant 
# pacman -S ant 
# dnf install ant 
# and so on
# Install adb android developement bridg
# It may be not needed to install the adb manualy if the android update command
# did work correctly
# Otherwise try to install it from distributions repos or clone the adb sources
#from git and compile them on your own.
# Pleas refere to the other info-files in this directory to sea how to use the
#installed stuff to install, build and deploy your own android apps.


