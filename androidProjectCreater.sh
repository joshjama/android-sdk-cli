#!/bin/bash
# This is a commandline UI to provide easy to use functionality of creating new
# android projects without android studio on the commandline
# It provides a question - awnser dialog so the user can give the required
# information to build a new android project without the need to remember all
# parameters needed by project creation
# 
# Requirements:
# java openjdk or oracle jdk v 8.0 or newer
# android sdk installed and available to execute in your $PATH
# apache-ant installed and in your $PATH
# adb installed an in your $PATH : mostly provided by android sdk
# 
# To create a new android project we have to know the following parameters
# before we can execute the creation programm:
# -a  : The name of the projects MainActivity.java The java class which is
# instanciated and executed by launching your app
# -k  : the java package name of the project for example
# lastname.firstname.activityToppic 
# -t  :  the android sdk api level tag for example android-27 for android
# version 9
# -p : the path to create the project inside for example
# $HOME/my_Application/ In the most cases it should be detected automaticaly 
# 
# Reading -a activity name from user input
READ_ActivityName() {
  echo "What's the name of your new MainActivity? "
  echo "It effects the later shown name on the phones"
  echo "so think a while before you type. "
  echo " The default is MainActivity "
  read
  if [[ $REPLY = " " ]] || [[ $REPLY = "" ]]
  then
    activityName="MainActivity"
  elif [[ $REPLY = "p" ]] || [[ $REPLY = "P" ]]
  then
    READ_ActivityName
  else
    activityName=$REPLY
  fi
  echo $activityName
}

# read java package name from user input
READ_JavaPackageName() {
  echo "What's the name of your java package for example that could be 
   lastName.firstName.applicationToppic default is $(whoami).android.example "
   echo " p  :  go back to the main activity name "
   read
  if [[ $REPLY = " " ]] || [[ $REPLY = "" ]]
  then
    javaPackageName=$(whoami)".android.example"
  elif [[ $REPLY = "p" ]] || [[ $REPLY = "P" ]]
  then
    javaPackageName="p"
    READ_ActivityName
    READ_JavaPackageName
  else 
    javaPackageName=$REPLY
  fi
  echo $javaPackageName
}

# read the android api level from user input
READ_AndroidApiLevel() {
  echo "What's the api level you want to use for your new project?"
  echo "Type \"levels\" to show available level tags"
  echo "default is android-27 for android version 8"
  echo "p  :  to go back to the java package name"
  read
  if [[ $REPLY = "" ]] || [[ $REPLY = " " ]]
  then
    androidApiLevel="android-27"
  elif [[ $REPLY = "p" ]] || [[ $REPLY = "P" ]]
  then
    androidApiLevel="p"
    READ_JavaPackageName
    READ_AndroidApiLevel
  elif [[ $REPLY = "levels" ]]
  then
    android list targets
    READ_AndroidApiLevel
  else
    androidApiLevel=$REPLY
  fi
  echo $androidApiLevel
}

# read android project path from user input
READ_AndroidProjectPATH() {
  echo "Where do you want to save your new android project? "
  echo "THe path has to be absolute"
  echo "The default is in " $(pwd)/$activityName
  read
  if [[ $REPLY = "" ]] || [[ $REPLY = " " ]]
  then
    mkdir -p $(pwd)/$activityName
    if [[ $? -eq 0 ]]
    then
      androidProjectPATH=$(pwd)/$activityName
    else 
      READ_AndroidProjectPATH
    fi
  elif [[ $REPLY = "p" ]] || [[ $REPLY = "P" ]]
  then
    androidProjectPATH="p"
    READ_AndroidApiLevel
    READ_AndroidProjectPATH
  else 
    androidProjectPATH=$REPLY
  fi
  echo $androidProjectPATH
}

# creat a new android project with the given parameters
createAndroidProject() {
  READ_ActivityName
  READ_JavaPackageName
  READ_AndroidApiLevel
  READ_AndroidProjectPATH
  echo $activityName " : " $javaPackageName " : " $androidApiLevel " : " $androidProjectPATH
  android create project -a $activityName -k $javaPackageName -t $androidApiLevel -p $androidProjectPATH
}

# Initialize the path of the android sdk tools
# Reading it from the config file if available 
initialize_AndroidPath() {
  if [[ -f $HOME/.androidProjectCreater/pathFile.conf ]]
  then
    source $HOME/.androidProjectCreater/pathFile.conf
  fi
  export PATH=$1:$PATH
  android list targets >> /dev/null
  if [[ $? -eq 0 ]]
  then
    echo "Congratulations! Your android developement tools seam to work fine "
  else 
    echo "Error : sdk-tools not found"
    echo ""Where did you store your sdk-tools ?
    echo "For example /home/user/sdk-tools/tools/ "
    echo "If installed by the setup script inside this repo the path is located in "
    echo $HOME"/.android-sdk/android-sdk-linux/tools/"
    echo "It should be detected automaticaly "
    echo "Press enter to try or provide your own path "
    read
    if [[ -d $REPLY ]]
    then
      export PATH=$PATH:$REPLY
    else 
      if [[ -d $HOME/.android-sdk/android-sdk-linux/tools ]]
      then
        PATH=$HOME/.android-sdk/android-sdk-linux/tools/:$PATH
      else 
        initialize_AndroidPath
      fi
    fi
  fi
  mkdir -p $HOME/.androidProjectCreater/
  if [[ -f $HOME/.android-sdk/pathFile.conf ]]
  then
    cat $HOME/.androidProjectCreater/pathFile.conf | sed 's/exp/#exp/g' > $HOME/.androidProjectCreater/.pathFile.tmp
  fi
  if [[ -f $HOME/.androidProjectCreater/.pathFile.tmp ]]
  then
    rm $HOME/.androidProjectCreater/pathFile.conf
    mv $HOME/.androidProjectCreater/.pathFile.tmp $HOME/.androidProjectCreater/pathFile.conf
  fi
  echo "export PATH="$PATH >> $HOME/.androidProjectCreater/pathFile.conf
}
#
# Print if the execution of the last command was successfull
print_SUCCESS() {
  if [ $? -eq 0 ]
  then
    echo "  success"
  else 
    echo "  fail"
  fi
}


# Main execution 
initialize_AndroidPath $1

if [[ $? -eq 0 ]]
then
  createAndroidProject
fi

print_SUCCESS
